extends Node2D

enum {MANUAL, U_MONKEY, T_MONKEY, CHILD, U_INTERN, F_COOKIES, 
	SOCIAL_MANAGER, COMMUNITY_MANAGER, MARKETING_DEPT, CELEBRITY }

# Statistics Tracking
@export var post_counts_by_category := {MANUAL: 0, U_MONKEY: 0, T_MONKEY: 0, CHILD: 0,
	U_INTERN: 0, F_COOKIES: 0, SOCIAL_MANAGER: 0, COMMUNITY_MANAGER: 0, MARKETING_DEPT: 0, CELEBRITY: 0}

# The three basic currencies for the game.
@export var post_count := 0
@export var like_count := 0
@export var follower_count := 0

# Other values to keep track of.
var backlog_power := 0
var debugging_info_active := false
var simulate_autoclicker := false

# Scratchpad for randomness
var next_posts := {MANUAL: [], U_MONKEY: [], T_MONKEY: [], CHILD: [],
	U_INTERN: [], F_COOKIES: [], SOCIAL_MANAGER: [], COMMUNITY_MANAGER: [], MARKETING_DEPT: [], CELEBRITY: []}


func _ready():
	randomize()
	Console.add_command("debugging_data", self, "toggle_debug_text").register()
	Console.add_command("autoclicker", self, "toggle_autoclicker").register()


func format_number_string(score_int : int) -> String:
	var score := str(score_int)
	var i : int = score.length() - 3
	while i > 0:
		score = score.insert(i, ",")
		i = i - 3
	return score


func get_like_count_str():
	return format_number_string(like_count)


func get_follower_count_str():
	return format_number_string(follower_count)


func toggle_debug_text():
	debugging_info_active = !debugging_info_active


func toggle_autoclicker():
	simulate_autoclicker = !simulate_autoclicker


# Track the necessary settings when creating a post.
func track_post_metrics(post: PostSettings):
	post_counts_by_category[post.Type] += 1
	post_count += 1
	if post.ContributesToBacklog:
		backlog_power += 1


func get_next_post(post: PostSettings):
	if next_posts[post.Type].size() == 0:
		next_posts[post.Type] += post.PostPhrases
		next_posts[post.Type].shuffle()
	
	var phrase = next_posts[post.Type][0]
	next_posts[post.Type].remove_at(0)
	return phrase


# Returns the power for the current post type.
func post_power(post: PostSettings):
	return int(post.Power * (1 + sqrt(follower_count * 0.001)))


# Returns the power of all posts on the backlog.
func old_post_power():
	return int(sqrt(backlog_power * 0.001) * 100)
