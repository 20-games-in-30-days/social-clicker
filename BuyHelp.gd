extends Button

var click_count := 0
var cost := 1000

@export var avatar: Resource
@export var settings: Resource
@export var text_color: Color
@export var disabled_color: Color
@export var mute_threshold := 10
var cooldown_time := INF

@onready var timer = $Timer
@onready var purchase_info = $PurchaseInfo
@onready var mute_button = $Mute

signal create_post


func _ready():
	$TextureRect.texture = avatar
	mute_button.visible = false
	calculate_New_Cost()


func _process(_delta):
	disabled = (cost > Global.like_count)
	if disabled:
		purchase_info.set("theme_override_colors/font_color", disabled_color)
	else:
		purchase_info.set("theme_override_colors/font_color", text_color)


func calculate_New_Cost():
	cost = int(settings.Cost * pow(1.15, click_count))
	text = settings.Name
	purchase_info.text = "Cost: %s likes (Have %s)   " % \
		[Global.format_number_string(cost), Global.format_number_string(click_count)]
	if click_count > 0:
		cooldown_time = settings.CooldownTime / float(click_count)
	if click_count >= mute_threshold:
		mute_button.visible = true


func load_game(times_purchased: int):
	click_count = times_purchased
	mute_button.visible = false
	
	calculate_New_Cost()
	if click_count == 0:
		timer.stop()
	elif timer.is_stopped():
		timer.start(cooldown_time)


func create_Post():
	timer.start(cooldown_time)
	emit_signal("create_post", settings)


func _on_BuyHelp_pressed():
	if cost < Global.like_count:
		Global.like_count -= cost
		click_count += 1
		calculate_New_Cost()
		create_Post()


func _on_Timer_timeout():
	create_Post()


func _on_Mute_pressed():
	settings.Mute = !settings.Mute

	if settings.Mute:
		mute_button.text = "Unmute"
	else:
		mute_button.text = "Mute  "
