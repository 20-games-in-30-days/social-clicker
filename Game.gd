extends CanvasLayer

@onready var DebuggingText = $Divider/Title/DebugInfo
@onready var PostManager = $Divider/CenterColumn/PostManager
@onready var PostContents = $Divider/CenterColumn/PostText
@onready var FollowerStats = $Divider/Title/Stats
@onready var PostButton = $Divider/CenterColumn/CenterContainer/PostButton
@export var manual_posts: Resource

@export var manual_post_dropoff := 1.1 # Posts loose 10% of their power each tick.
@export var auto_post_dropoff := 1.1 # Posts loose 10% of their power each tick.
@export var post_view_modifier := 600 # 1 old post converts to one view every 60 seconds.
@export var follwer_conversion_rate := 87 # Every 87 views will generate one new follower.

var manual_post_power := 0
var auto_post_power := 0

# Scratch variables in the conversion from posts to views and views to followers.
var reach_remainder := 0
var follower_remainder := 0


func _process(_delta):
	if Global.simulate_autoclicker:
		_on_Post()


func _ready():
	await get_tree().process_frame
	load_game()


func _input(event):
	if event.is_action_pressed("Enter") and !PostButton.has_focus():
		_on_create_manual_post()


func _physics_process(_delta):
	# 10 times per second. Do our calculations here for a fixed framerate.

	# Calcuate the view count for outstanding posts. Start with carryover from the last tick.
	var post_reach := reach_remainder

	# Add one for every post ever made.
	post_reach += Global.old_post_power()
	
	# Add the new post's power.
	post_reach += int(manual_post_power + auto_post_power)

	# Calculate the number of views generated this tick, then store the remainder.
	reach_remainder = post_reach % post_view_modifier
	var new_likes = post_reach / post_view_modifier
	Global.like_count += new_likes
	
	# Calcuate the number of new followers from views.
	var new_followers = follower_remainder
	new_followers += new_likes
	
	follower_remainder = new_followers % follwer_conversion_rate
	Global.follower_count += new_followers / follwer_conversion_rate
	
	# Drop our new post power down towards zero.
	manual_post_power = int(max(manual_post_power / manual_post_dropoff, 0))
	auto_post_power = int(max(auto_post_power / auto_post_dropoff, 0))
	
	FollowerStats.text = "[color=#00bdff][b]You[/b][/color] have " + \
	"[color=#00bdff]%s[/color] likes\n" % Global.get_like_count_str() + \
	" and [color=#00bdff]%s[/color] followers!" % Global.get_follower_count_str()
	
	if Global.debugging_info_active:
		DebuggingText.text = "\n\n\nPosts: %s\n\n\n" % Global.format_number_string(Global.post_count) + \
			"Manual Posts: %s\nNew Post Power %s\nOld Post Power %s;\nTotal Post Power %s\n" % \
			[Global.format_number_string(Global.post_counts_by_category[Global.MANUAL]), \
			Global.format_number_string(manual_post_power), Global.format_number_string(Global.old_post_power()), \
			Global.format_number_string(manual_post_power + Global.old_post_power()) ] + \
			"\n\nAuto Post Power: %s\n" % Global.format_number_string(auto_post_power) + \
			"Automatic Post Count: %s\n" % Global.format_number_string(Global.post_count - Global.post_counts_by_category[Global.MANUAL]) + \
			"   U. Monkey %d \n   T. Monkey %d \n   Child %d \n   English Int. %d \n   Marketing Int. %d \n   Social Man. %d \n   Community Man. %d \n   Marketing Dept. %d \n   Celebrity Shoutout. %d" % \
			[Global.post_counts_by_category[Global.U_MONKEY], Global.post_counts_by_category[Global.T_MONKEY], Global.post_counts_by_category[Global.CHILD], \
			Global.post_counts_by_category[Global.U_INTERN], Global.post_counts_by_category[Global.F_COOKIES], Global.post_counts_by_category[Global.SOCIAL_MANAGER], \
			Global.post_counts_by_category[Global.COMMUNITY_MANAGER],Global.post_counts_by_category[Global.MARKETING_DEPT], Global.post_counts_by_category[Global.CELEBRITY]]
	else:
		DebuggingText.text = ""


func _on_create_auto_post(settings: PostSettings):
	Global.track_post_metrics(settings)
	if !settings.Mute:
		PostManager.post(Global.get_next_post(settings), settings.PostColor)
	
	auto_post_power = int(auto_post_power + Global.post_power(settings))


func _on_create_manual_post():
	if (PostContents.text == ""):
		get_new_manual_phrase()
	Global.track_post_metrics(manual_posts)
	PostManager.post(PostContents.text, manual_posts.PostColor)
	
	manual_post_power = int(min(manual_post_power + Global.post_power(manual_posts), Global.post_power(manual_posts) * 10))

	if PostContents.has_focus():
		PostContents.text = ""
	else:
		get_new_manual_phrase()


func save_game():
	var saveData := {
		"followers" : Global.follower_count,
		"likes" : Global.like_count,
		"posts" : Global.post_count,
		"posts_by_category" : Global.post_counts_by_category,
		"backlog_power" : Global.backlog_power,
		"helpers" : []
	}
	
	for buyHelp in $Divider/BuyHelp.get_children():
		if buyHelp is Button:
			saveData["helpers"].append(buyHelp.click_count)
	
	Print.info("Saving Game\n" + str(saveData))
	
	var saveGame = FileAccess.open("user://savegame.save", FileAccess.WRITE)
	if saveGame:
		saveGame.store_line(JSON.stringify(saveData))
		saveGame.close()
	else:
		Print.error("Failed to open file for writing.")


func reset_game():
	Global.follower_count = 0
	Global.like_count = 0
	Global.post_count = 0
	for category in Global.post_counts_by_category.keys():
		Global.post_counts_by_category[category] = 0
	Global.backlog_power = 0
	
	manual_post_power = 0
	auto_post_power = 0
	
	for buyHelp in $Divider/BuyHelp.get_children():
		if buyHelp is Button:
			buyHelp.load_game(0)
			
	PostManager.reset()


func load_game():
	if not FileAccess.file_exists("user://savegame.save"):
		Print.error("There was no save file to load!")
		return # Error! We don't have a save to load.

	var saveGame = FileAccess.open("user://savegame.save", FileAccess.READ)
	if saveGame:
		var test_json_conv = JSON.new()
		test_json_conv.parse(saveGame.get_line())
		var saveData = test_json_conv.get_data()
		Global.follower_count = saveData["followers"]
		Global.like_count = saveData["likes"]
		Global.post_count = saveData["posts"]
		Global.backlog_power = saveData["backlog_power"]

		# Needs special attention. Convert from str to int.
		Global.post_counts_by_category = {}
		for key in saveData["posts_by_category"].keys():
			Global.post_counts_by_category[int(key)] = saveData["posts_by_category"][key]
		
		var helpers: Array = saveData["helpers"]
		for buyHelp in $Divider/BuyHelp.get_children():
			if buyHelp is Button:
				if helpers.size() <= 0:
					return
				buyHelp.load_game(helpers[0])
				helpers.remove_at(0)
			saveGame.close()
	else:
		Print.error("Failed to open file for reading.")


func get_new_manual_phrase():
	PostContents.text = Global.get_next_post(manual_posts)


func _on_Post():
	_on_create_manual_post()


func _on_PostText_focus_entered():
	PostContents.text = ""


func _on_Save_Timer_timeout():
	save_game()


func _on_Clear_Progress_pressed():
	reset_game()
