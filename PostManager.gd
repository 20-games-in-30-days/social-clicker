extends Container

@onready var first_post = $Post1
var posts = []


func _ready():
	posts = get_children()
	for i in (posts.size() - 1):
		posts[i-1].next_post = posts[i]


func post(text: String, color: Color):
	first_post.post(text, color)


func reset():
	for p in posts:
		p.clear()
