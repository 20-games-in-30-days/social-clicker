extends Resource
class_name PostSettings

@export var Type := 0
@export var Name := "Post"
@export var PostColor := Color.WHITE
@export var Power := 1000
@export var Cost := 1000
@export var CooldownTime := 5
@export var ContributesToBacklog := false
@export var Mute := false
@export var PostPhrases := ["Hello, World!"] # (Array, String, MULTILINE)
