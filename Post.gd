extends Control

@export var post_height = 100
@export var color := Color.WHITE
@export var gradient : Gradient
@onready var label = $Label

var next_post = null


func _ready():
	# Ensure that the gradient is unique
	$Background.color = color
	color = Color.TRANSPARENT
	gradient = gradient.duplicate()
	$Highlighting.texture = GradientTexture2D.new()
	$Highlighting.texture.gradient = gradient


func post(text: String, my_color: Color):
	cascade(text, my_color)


func cascade(text: String, my_color: Color):
	if next_post and label.text != "":
		next_post.cascade(label.text, color)
	
	color = my_color
	gradient.colors[0] = color
	gradient.colors[3] = color
	
	custom_minimum_size.y = post_height
	label.text = text


func clear():
	custom_minimum_size.y = 0
	label.text = ""
